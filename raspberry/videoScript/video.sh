#! /bin/bash

# ffmpeg \
#     -i rtsp://192.168.42.1/live \
#     -vcodec copy \
#     -acodec copy \
#     -f flv rtmp://54.37.153.179:1935/live/test

# general format
# ffmpeg -i "[rtsp link]" -f flv -r [fram-rate] -s [scale] -an "[rtmp link]"

# add the rtsp and rtmp urls and specify the frame rate and output resolution

# drone:
#rtsp_url="rtsp://192.168.42.1/live"
# simulation:
rtsp_url="rtsp://10.202.0.1/live"
rtmp_url="rtmp://54.37.153.179:1935/live/test"
fps=25
res="640x480"

ffmpeg -i $rtsp_url -f flv -r $fps -s $res -an $rtmp_url