#!/usr/bin/env python

import asyncio
import websockets
import threading


####### SERVER PART ###########################
# get telemetry from ESP32
telemetry = "None"


async def getCoords(websocket) -> str:
    global telemetry
    coords = await websocket.recv()
    telemetry = coords
    # print("< {}".format(coords))
    greeting = "coord received"
    await websocket.send(greeting)
    return coords


async def mainServer():
    async with websockets.serve(
        getCoords, "10.33.16.137", 8765
    ):  # remplacer par ip Raspberry
        await asyncio.Future()  # run forever


def websocket_server_run():
    asyncio.run(mainServer())


####################################################

############### CLIENT PART ###############################
async def sendCoords():
    global telemetry
    async with websockets.connect("ws://54.37.153.179:8765") as websocket:
        strCoords = telemetry
        await websocket.send(strCoords)
        print(f"---> {strCoords}")

        greeting = await websocket.recv()
        print(f"<--- {greeting}")


async def mainClient():
    async with websockets.serve(
        sendCoords, "54.37.153.179", 8765
    ):  # remplacer par ip Raspberry
        await asyncio.Future()  # run forever


def websocket_client_run():
    asyncio.run(mainClient())


######################################################


############## RUN #############
WS_server_thred = threading.Thread(target=websocket_server_run)
WS_client_thred = threading.Thread(target=websocket_client_run)

WS_server_thred.start()
WS_client_thred.start()
###############################
