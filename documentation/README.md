
# Projet Drone sécuritée IA 

## Sommaire

- [Intro](#intro-et-prérequis)

- [IHM](#interface-humain-machine)
  - [Serveur Python Flask Socket.io](#serveur-python-flask-socket-io)
  - [Serveur WebSocket](#serveur-websocket)
  - [Serveur RTMP](#serveur-rtmp)
  - [Base de Donnees](#base-de-donnees)
  - [Nginx](#nginx)
  - [Liens utile](liens-utile)

## Intro et prérequis 

Ici vous trouverez le détail d'installation et de configuration du projet Drone de sécurité. 

Ce projet a pour but de concevoir une solution de surveillance grace à un drone autopiloter capable de reconnaître le visage des personnes autoriser à se trouver dans la zone de surveillance. 

- **Carte Electronique ESP32** <br>
Gestion de la detection de présence dans  la zone de surveillance
  - **Capteurs a ultrasons** <br>
  Pour la détection et localisation de mouvement
  - **Websocket** <br>
  Transfère des données de localisation vers RaspberryPi

- **Drone Parrot Anafi** <br>
Se rendre sur place seul, capter le visage et si aucune correspondance lancr l'alerte
  - **Stream de la caméra**
  - **Stream de la télémetrie**
  - **Reception du plan de vol**

- **Base de données** <br>
Stockant les visages des personnes autoriser, la télémetrie, les User/Passwd du personnel autoriser a se connecter sur le site web.
  - **visages**
  - **Users**
  
- **RaspberryPi** <br>
Element centrale permettant de recevoir les données des capteurs et du drone. Traiter les données (faceRecognition,   DroneDriver, AlertLauncher) et de transmettre au drone le plan de vole, au serveur web, base de donnée les données traiter.
  - **Streaming video drone** <br>
  Récuperer le flux vidéo du drone
  - **Websocket** <br>
  Récuperer les données capteurs de ESP32
  - **FaceRecognition** <br>
  Traiter le flux vidéo du drone 
  - **Drone Driver** <br>
  Piloter le drone grace au données du drone et de l'ESP32
  - **AlertLauncher** <br>
  Si visage non reconnu lancer l'alerte
  - **Base de données**
- **VPS OVH** <br>
Affichage web des données du drone et managment des utilisateurs et personnes autoriser.
  - **Serveur web Flask** <br>
  Recoit le flux vidéo et la télémetrie en temps réél du drone depuis la RaspberryPi. <br>
  Transmet localement à NGINX (revers Proxy pour https et repartition de charge)
  - **Users Manager**<br>
  Manager des users autoriser a se connecter sur le site web et des personnes reconu par le drone.<br>
  Recois de la base de données les donnée Users et du serveur flask les input des clients web <br>
  Emmet au serveur Flask les données Users
  - **NGINX** <br>
  Recoit les données a afficher sur le site web depuis le serveur Flask .<br>
  Transmet les requet des clients web au serveur Flask
- **PC-Client** <br>
récupère les données de télémetrie et vidéo du drone en direct 
  - **Navigateur Web** <br>
  Récupère les pages web servie par NGINX. <br>
  Transmet les requetes client à NGINX. <br>

![shema](./architectureLogiciel_ProjetDrone.drawio-2.png)

## Comunications: 
- **vidéo:** RTSP et RTP. 
- **Programe/Machine:** Websockets avec ssl pour l'encryptage.
- **Base de donnée mysql:** mysql connectors

## Interface Humain Machine

### Serveur Python Flask Socket io

Les fichiers [serverFflask.py](./web_server/serverFlask.py), [databaseManager.py](./web_server/databaseManager.py)  et [videoFeed.py](./web_server/videoFeed.py) sont le point centrale de l'HIM ils permettent de rassembler dans un web serveur le flux video provenant du serveur RTMP NGINX, du serveur Websocket pour les servir en local a l'addresse 127.0.0.1:5000 afin de permettre aux utilisateurs de visualiser le flux vidéo du drone ainsi que sa télémétrie.

le serveur socket.io : 

```py
import threading
import databaseManager as dbManager
import videoFeed
import asyncio
from multiprocessing.connection import wait
from time import sleep
import websockets
from flask import (
    Flask,
    render_template,
    request,
    url_for,
    redirect,
    Response,
    session,
    copy_current_request_context,
    make_response,
)
from threading import Lock
from flask_socketio import SocketIO, emit, disconnect

## ------- Global Var ------- ##

# socket.io & thread
async_mode = None
app = Flask(__name__)
app.config["SECRET_KEY"] = "secret!"
socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock()

## suite du code ##


if __name__ == "__main__":
    socketio.run(app, "54.37.153.179", "5000")
```

### Serveur WebSocket

Il est executer sur le `VPS` a l'ip `54.37.153.179:8765` dans un thread a l'interieur du serveur Flask.
Le serveur WebSocket nous permet de récuprer en temps réel les données des capteurs relier à l'ESP32. afin de les afficher en live via Socket.io sur la page `video.html`.

```py
async def getCoords(websocket) -> str:
    global telemetry
    coords = await websocket.recv()
    telemetry = coords
    greeting = "coord received"
    await websocket.send(greeting)
    return coords


async def main():
    async with websockets.serve(getCoords, "54.37.153.179", 8765):
        await asyncio.Future()  # run forever


def websocket_run():
    asyncio.run(main())
```

### Serveur RTMP
le serveur RTMP a pour but de recevoir le flux vidéo RTSP provenant du drone et de le servire a [videoFeed.py](./web_server/videoFeed.py) afin de permettre l'affichage du flux vidéo dans le navigateur.

  #### configurtation

  - installation du module:

  ```sh
  sudo apt update
  sudo apt install libnginx-mod-rtmp
  ```
  - Ajout au fichier de configuration:

  ```sh
  sudo nano /etc/nginx/nginx.conf
  ```
  ```sh
. . .
rtmp {
        server {
                listen 1935;
                chunk_size 4096;
                allow publish 127.0.0.1;
                deny publish all;

                application live {
                        live on;
                        record off;
                }
        }
}
  ```

- Actualisation des règles du par-feu:

```sh
sudo ufw allow 1935/tcp
```

- Redémarage de NGINX:

```sh
sudo systemctl reload nginx.service
```


### Base de Donnees

#### installation
Installation de Mariadb serveur et client.
```bash
paja444@SRVInfra:~$ sudo apt-get install curl software-properties-common dirmngr
```
```bash
paja444@SRVInfra:~$ curl -LsS -O https://downloads.mariadb.com/MariaDB/mariadb_repo_setup
paja444@SRVInfra:~$ sudo bash mariadb_repo_setup --os-type=debian  --os-version=buster --mariadb-server-version=10.6
paja444@SRVInfra:~$ wget http://ftp.us.debian.org/debian/pool/main/r/readline5/libreadline5_5.2+dfsg-3+b13_amd64.deb
paja444@SRVInfra:~$ sudo dpkg -i libreadline5_5.2+dfsg-3+b13_amd64.deb
```
```bash
paja444@SRVInfra:~$ sudo apt-get update
paja444@SRVInfra:~$ sudo apt-get install mariadb-server mariadb-client
```
```bash
paja444@SRVInfra:~$ sudo systemctl start mariadb
paja444@SRVInfra:~$ sudo systemctl enable mariadb
```
```bash
paja444@SRVInfra: sudo mysql_secure_installation
```
```bash
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none): 
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] Y
Enabled successfully!
Reloading privilege tables..
 ... Success!


You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] Y
New password: 
Re-enter new password: 
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] Y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] Y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```
#### configuration
```bash
paja444@SRVInfra:~$ sudo ufw allow 3306
```
```bash
paja444@SRVInfra:~$ mysql -u root -p
```
Création de le base de données.
```bash
MariaDB [(none)]> CREATE DATABASE serverDroneSecu ;
```
```bash
MariaDB [(none)]> use serverDroneSecu              
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [serverDroneSecu]> 
```
Création de la table "Users".
```bash
CREATE TABLE Users (
Uuid TEXT,
UserName TEXT,
Password TEXT
);
```
Création des utilisateurs de la base.

user admin pour la gestion.
```bash
CREATE USER 'admin'@'54.37.153.179' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON serverDroneSecu.* TO 'admin'@'54.37.153.179';
FLUSH PRIVILEGES;
```

#### Installation de mariaDB connector

MariaDB Connector/Python est une api permettant la communication entre un programme Python et la base de données Mariadb.

```bash
paja444@SRVInfra:~$ pip3 install mariadb
```

Une fois la base de donnée installer et configurer: 

le fichier [databaseManager.py](./web_server/databaseManager.py) nous permet d'implementer les fonctions nécessaire a l'utilisation de notre nouvelle base de donnée dans notre programe.

Afin de chiffrer nos mots de passe dans la base de donnée nous allons utiliser `bcrypt` et ces deux fonctions` <br>

```py
def hashPassword(passwd: str):

    return bcrypt.hashpw(passwd.encode(), bcrypt.gensalt())


def checkPassword(passwd: str, hash) -> bool:
    return bcrypt.checkpw(passwd.encode(), hash.encode())
```




### Nginx

NGINX vas nous permettre de faire configurer un revers proxy afin de sécuriser notre site web.

#### configuration

  - creer un service executant notre web serveur Flask socket.io:

  ```bash
sudo nano /etc/systemd/system/droneSecu.service
  ```
  ```bash
[Unit]
Description= Flask server for droneSecu Service
ConditionPathExists=/home/paja444/server_dronesecu/web_server/
After=network.target
[Service]
Type=simple
User=paja444
Group=paja444
WorkingDirectory=/home/paja444/server_dronesecu/web_server/
ExecStart=python3 serverFlask.py
Restart=on-failure
RestartSec=10
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=serverDroneSecuService
[Install]
WantedBy=multi-user.target
  ```
  - créer un fichier de configuration nginx:

  ```bash
sudo nano /etc/nginx/conf.d/droneSecu.conf
  ```
  ```bash
upstream backend_flask{
  ip_hash;
  server 127.0.0.1:5000 weight=3;
}

server {
    listen 80;
    return 301 https://$host$request_uri;
}

limit_req_zone $binary_remote_addr zone=mylimit:100m rate=100r/s;
server {
  listen 443 ssl;

    ssl_certificate           /etc/letsencrypt/live/alpagam.com/fullchain.pem;
    ssl_certificate_key       /etc/letsencrypt/live/alpagam.com/privkey.pem;
    ssl_trusted_certificate   /etc/letsencrypt/live/alpagam.com/chain.pem;
    server_name www.alpagam.com alpagam.com;
  location / {
     include proxy_params;
     limit_req zone=mylimit burst=20 nodelay;
     proxy_pass http://backend_flask;
  }
}
  ```

  - Générer un certificat ssl afin de passer notre site en https:
```bash
sudo snap install --classic certbot
```
```bash
sudo ln -s /snap/bin/certbot /usr/bin/certbot
```
  ```bash
sudo certbot certonly --nginx
  ```
- Redémarage de NGINX:

```bash
sudo systemctl reload nginx.service
```

## Liens utile

- SSL : https://docs.python.org/3/library/ssl.html#module-ssl
- Websockets : https://websockets.readthedocs.io/en/latest/intro/quickstart.html#encrypt-connections
- Protocole RTSP/RTP : https://www.iifa.fr/video-ip / https://github.com/aler9/rtsp-simple-server#from-a-webcam
- IPC : https://www.geeksforgeeks.org/inter-process-communication-ipc/
- Drone : https://developer.parrot.com/docs/groundsdk-tools/overview.html <br>
          https://developer.parrot.com/docs/olympe/olympeapi.html?highlight=websocket

