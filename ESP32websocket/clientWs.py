#!/usr/bin/env python

import asyncio
from multiprocessing.connection import wait
from time import sleep
import websockets
import random


def randCoords() -> str:
    dataDictionary = {
        "Latitude": 44.83090627635009,
        "Longitude": -0.608351824901316,
        "Angle": 60,
        "Distance": 0,
    }
    dataDictionary["Distance"] = random.randrange(0, 100)
    return dataDictionary


async def sendCoords():
    async with websockets.connect("ws://54.37.153.179:8765") as websocket:
        strCoords = str(randCoords())
        await websocket.send(strCoords)
        print(f"---> {strCoords}")

        greeting = await websocket.recv()
        print(f"<--- {greeting}")


while True:
    asyncio.get_event_loop().run_until_complete(sendCoords())
    sleep(3)
