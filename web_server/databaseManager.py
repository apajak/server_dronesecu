import bcrypt
import mariadb
import sys
import uuid

from serverFlask import User

cursor = None
serverDroneSecuDB = None


def getConnection():

    global cursor
    global serverDroneSecuDB
    # Connect to MariaDB Platform
    try:
        serverDroneSecuDB = mariadb.connect(
            user="admin",
            password="admin",
            host="localhost",
            port=3306,
            database="serverDroneSecu",
        )
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)

    # Get Cursor
    cursor = serverDroneSecuDB.cursor()


def addUserToDB(userName: str, passwd: str):
    getConnection()
    global cursor
    global serverDroneSecuDB
    hash = hashPassword(passwd)
    password = hash.decode()
    try:
        cursor.execute(
            f'INSERT INTO Users (Uuid,UserName,Password) VALUES ("{str(uuid.uuid4())}","{userName}","{password}");'
        )
        print("Successfully added user to database")

    except mariadb.Error as e:
        print(f"Error adding user to database: {e}")
    serverDroneSecuDB.commit()
    serverDroneSecuDB.close()


def UpdateDBUser(name, password):
    getConnection()
    global cursor
    global serverDroneSecuDB
    try:
        cursor.execute(
            f"update Users set password = '{password}' where name = '{name}';"
        )
        print("Successfully update entry to database")
    except mariadb.Error as e:
        print(f"Error update entry to database: {e}")
    serverDroneSecuDB.commit()
    serverDroneSecuDB.close()


def getDBUserList() -> list:
    getConnection()
    global cursor
    global serverDroneSecuDB
    global UserList
    UserList = []
    try:
        cursor.execute("SELECT * FROM Users;")
        for user in cursor:
            UserList.append(user)
        print("Successfully load database")
        return UserList
    except mariadb.Error as e:
        print(f"Error adding entry to database: {e}")

    serverDroneSecuDB.commit()
    serverDroneSecuDB.close()


def rmUserToDB(user):
    global cursor
    global serverDroneSecuDB
    getConnection()
    id = user.id
    try:
        cursor.execute(f"delete from Users where Uuid = '{id}';")
        print("Successfully rm entry to database")
    except mariadb.Error as e:
        print(f"Error adding entry to database: {e}")
    serverDroneSecuDB.commit()
    serverDroneSecuDB.close()


def hashPassword(passwd: str):

    return bcrypt.hashpw(passwd.encode(), bcrypt.gensalt())


def checkPassword(passwd: str, hash) -> bool:
    return bcrypt.checkpw(passwd.encode(), hash.encode())


def getDBUser(uuid):
    getConnection()
    global cursor
    global serverDroneSecuDB
    global UserList
    try:
        cursor.execute(f"SELECT * FROM Users where Uuid = '{uuid}';")
        row = cursor.fetchone()
        user = User(row[0], row[1], row[2])
        print("Successfully load database")
        return user
    except mariadb.Error as e:
        print(f"Error adding entry to database: {e}")

    serverDroneSecuDB.commit()
    serverDroneSecuDB.close()
