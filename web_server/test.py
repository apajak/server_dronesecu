def getTelemetrieData(data: str) -> list:
    dataDictionary = {"Latitude": 0, "Longitude": 0, "Angle": 0, "Distance": 0}
    dataTab = data.split(",")
    dataDictionary["Latitude"] = float(dataTab[0].split(":")[1])
    dataDictionary["Longitude"] = float(dataTab[1].split(":")[1])
    dataDictionary["Angle"] = int(dataTab[2].split(":")[1])
    dataDictionary["Distance"] = int(dataTab[3].split(":")[1])
    return dataDictionary


def checkTelemetrieData(dataDictionary: dict) -> bool:
    if dataDictionary["Distance"] < 20:
        return True
    else:
        return False


print(
    checkTelemetrieData(
        getTelemetrieData(
            "Latitude : 44.83090627635009 , Longitude : -0.608351824901316 , Angle : 60 , Distance : 30 "
        )
    )
)
