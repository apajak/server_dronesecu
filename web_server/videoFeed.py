import cv2


def gen_frames():  # generate frame by frame from camera
    global cap_flag
    # webcam VideoStream
    video_capture = cv2.VideoCapture("rtmp://54.37.153.179/live/test")
    # video_capture = cv2.VideoCapture("udp://192.168.42.1")
    # video_capture = cv2.VideoCapture(0)

    while True:
        # Capture frame-by-frame
        ret, frame = video_capture.read()  # read the camera frame
        if not ret:
            break
        else:
            ret, buffer = cv2.imencode(".jpg", frame)
            frame = buffer.tobytes()
            yield (
                b"--frame\r\n" b"Content-Type: image/jpeg\r\n\r\n" + frame + b"\r\n"
            )  # concat frame one by one and show result
