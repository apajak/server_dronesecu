import threading
import databaseManager as dbManager
import videoFeed
import asyncio
from multiprocessing.connection import wait
from time import sleep
import websockets
from flask import (
    Flask,
    render_template,
    request,
    url_for,
    redirect,
    Response,
    session,
    copy_current_request_context,
    make_response,
)
from threading import Lock
from flask_socketio import SocketIO, emit, disconnect

## ------- Global Var ------- ##

# socket.io & thread
async_mode = None
app = Flask(__name__)
app.config["SECRET_KEY"] = "secret!"
socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock()

# Send telemetrie
telemetry = "Waiting..."
droneTakeOff = False

# user class for DB
class User:
    def __init__(self, id: int, name: str, passwd: str):
        self.id = id
        self.name = name
        self.passwd = passwd


## --------------------------- ##

## ------- DEF  ------- ##
def getUserList() -> list:
    UserList = []
    dbUserList = dbManager.getDBUserList()
    for dbUser in dbUserList:
        user = User(dbUser[0], dbUser[1], dbUser[2])
        UserList.append(user)
    return UserList


def getCurrentUser() -> User:
    sessionID = request.cookies.get("session")
    if sessionID != None:
        user = dbManager.getDBUser(sessionID)
    else:
        user = None
    return user


def getTelemetrieData(data: str) -> list:
    dataDictionary = {"Latitude": 0, "Longitude": 0, "Angle": 0, "Distance": 0}
    dataTab = data.split(",")
    dataDictionary["Latitude"] = float(dataTab[0].split(":")[1])
    dataDictionary["Longitude"] = float(dataTab[1].split(":")[1])
    dataDictionary["Angle"] = int(dataTab[2].split(":")[1])
    dataDictionary["Distance"] = int(dataTab[3].split(":")[1])
    return dataDictionary


def checkTelemetrieData(dataDictionary: dict) -> bool:
    if dataDictionary["Distance"] < 20:
        return True
    else:
        return False


## --------------------------- ##

## ----------------- HTML ROUTE --------------------##

# Video streaming route.
@app.route("/video_feed")
def video_feed():
    return Response(
        videoFeed.gen_frames(),
        mimetype="multipart/x-mixed-replace; boundary=frame",
    )


# Display UserManager route
@app.route("/faceManager", methods=["POST", "GET"])
def faceManager():
    currentUser = getCurrentUser()
    if currentUser != None:
        resp = make_response(render_template("faceManager.html", user=currentUser))
        return resp
    else:
        return redirect(url_for("login"))


# Display UserManager route
@app.route("/settings", methods=["POST", "GET"])
def userManager():
    currentUser = getCurrentUser()
    UserList = getUserList()
    if currentUser != None:
        resp = make_response(render_template("settings.html", user=currentUser))
        UserList = getUserList()
        if request.method == "POST":
            if request.form["submit"] == "logout":
                response = make_response(render_template("index.html"))
                response.set_cookie("session", currentUser.id, max_age=0)
                return response
            if request.form["submit"] == "add":
                userImput = request.form.get("userName")
                passwdImput = request.form.get("userPasswd")
                if userImput and passwdImput:
                    print("adduUser")
                    dbManager.addUserToDB(userImput, passwdImput)
                    return redirect(url_for("userManager"))
            for user in UserList:
                userToRemove = f"remove {user.name}"
                if request.form["submit"] == userToRemove:
                    dbManager.rmUserToDB(user)
                    return redirect(url_for("userManager"))
        return render_template("settings.html", UserList=UserList)
    else:
        return redirect(url_for("login"))


# Display Video streaming route
@app.route("/video", methods=["POST", "GET"])
def video():
    global cap_flag
    currentUser = getCurrentUser()
    if currentUser != None:
        resp = make_response(render_template("video.html", user=currentUser))
        if request.method == "POST":
            if request.form["submit"] == "logout":
                response = make_response(render_template("index.html"))
                response.set_cookie("session", currentUser.id, max_age=0)
                return response
            if request.form["submit"] == "cap":
                cap_flag = True
        return resp
    else:
        return redirect(url_for("login"))


# Home page route
@app.route("/home", methods=["POST", "GET"])
def home():
    currentUser = getCurrentUser()
    if currentUser != None:
        print(currentUser.name)
        print(currentUser.id)
        print(currentUser.passwd)
        resp = make_response(render_template("home.html", user=currentUser))
        if request.method == "POST":
            if request.form["submit"] == "logout":
                response = make_response(render_template("index.html"))
                response.set_cookie(
                    "session", currentUser.id, max_age=0
                )  # remove user cookie
                return response
    else:
        return redirect(url_for("login"))


# Welcome page route
@app.route("/", methods=["POST", "GET"])
def login():
    UserList = getUserList()
    Affiche = ""
    if request.method == "POST":
        print(UserList)
        userImput = request.form.get("name")
        passwdImput = request.form.get("password")
        for user in UserList:
            hash = user.passwd
            if userImput == user.name and dbManager.checkPassword(passwdImput, hash):
                currentUser = user
                response = make_response(render_template("home.html", user=user))
                response.set_cookie("session", currentUser.id)
                return response
            else:
                Affiche = "Wrong user or password."

    return render_template("index.html", Affiche=Affiche)


## -------- THREADS -------- ##

# thread Socket.io for live display telemetry in video.html
def background_thread():
    WS_thred.start()
    count = 0
    while True:
        global telemetry
        global droneTakeOff
        dataDictionary = getTelemetrieData(telemetry)
        if checkTelemetrieData(dataDictionary):
            droneTakeOff = True
            print(droneTakeOff)
        else:
            droneTakeOff = False
            print(droneTakeOff)

        socketio.sleep(1)
        count += 1
        socketio.emit("my_response", {"data": telemetry, "count": count})


async def getCoords(websocket) -> str:
    global telemetry
    coords = await websocket.recv()
    telemetry = coords
    greeting = "coord received"
    await websocket.send(greeting)
    return coords


async def main():
    async with websockets.serve(getCoords, "54.37.153.179", 8765):
        await asyncio.Future()  # run forever


def websocket_run():
    asyncio.run(main())


## ------------------------- ##

## -------- Socket.io -------- ##

# Socketio event for connect client:
@socketio.event
def connect():
    global thread
    print("connected")
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(
                background_thread
            )  # Start send telemetrie thread
    emit("my_response", {"data": "Connected", "count": 0})


# Socketio event for check disconnect is ok :
@socketio.event
def disconnect_request():
    @copy_current_request_context
    def can_disconnect():
        disconnect()

    session["receive_count"] = session.get("receive_count", 0) + 1
    # for this emit we use a callback function
    # when the callback function is invoked we know that the message has been
    # received and it is safe to disconnect
    emit(
        "my_response",
        {"data": "Disconnected!", "count": session["receive_count"]},
        callback=can_disconnect,
    )


# Socketio event for  disconnect Client :
@socketio.on("disconnect")
def test_disconnect():
    print("Client disconnected", request.sid)


## ------------------------------------ ##

WS_thred = threading.Thread(target=websocket_run)

if __name__ == "__main__":
    socketio.run(app, "127.0.0.1", "5000")
