#! /bin/bash

ffmpeg -f avfoundation -list_devices true -i ""
ffmpeg \
	-f avfoundation \
	-framerate 30 \
	-video_size 640x480 \
	-i "0:none" \
	-vcodec libx264 \
	-preset ultrafast \
	-tune zerolatency \
	-pix_fmt yuv422p \
	-f mpegts udp://localhost:12345  
